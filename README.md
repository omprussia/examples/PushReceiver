# Push Receiver

The application allows you to receive and process push notifications.

It can be used both as an example of the API interaction
(notification receiving and displaying, launching the application from the background, and
switching the UI mode) and for utilitarian purposes for testing and debugging the infrastructure.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/PushReceiver/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/PushReceiver/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/PushReceiver/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/PushReceiver/-/commits/dev)

## UX

When receiving a notification from the push-server, the first step is to check whether the
`PushReceiver` is currently running. If it is not running, then it is running in the
background (without GUI). Next, all notifications delivered to the device are received via
the `Push-daemon API` and graphical representations are created for each of them via
the `Notifications API`. If the application is running in the background and the notifications haven't been received for it for some time, then it shuts down. The graphical
representation of notifications can either be displayed as a pop-up or placed in the
notification curtain. Clicking on any of them will either launch the GUI application, if it has not been launched, or a graphical interface will be displayed
for the current process.

The application itself is a single page with two sections. The upper part is a list of
push-notifications received by this instance of the application and a header that displays
the number of notifications displayed. The `Application ID` and `Registration ID` are
displayed at the bottom. The application also has an upper pull-out menu which can be used to clear the push notification model.

## Preparation for work

To configure the entire system, including the push-server and applications for sending and
receiving notifications, you will need to perform several steps.
- The first step is to send a letter to developer support at [dev-support@omp.ru](dev-support@omp.ru),
where to tell the name of the application and request an archive with two configuration `yaml` files.
In the future, you will need a file with such content (also, its name is shorter):
  ```
    push_notification_system:
      project_id: "..."
      push_public_address: "..."
      api_url: "..."
      client_id: "..."
      scopes: "..."
      audience: "..."
      token_url: "..."
      key_id: "..."
      private_key: "..."
  ```

- The next step is to download the source code of the `PushReceiver` application. For this
application to work with the push-server, you need to set the `Application ID`. There are
two ways to do this: either in the `pro` project file assign the variable
[`APP_ID`](ru.auroraos.pushreceiver.pro#L46) you need (for example:
`APP_ID = orgexampleappname_fc7dfs3asd9fdgsa0`), or after installing the application on the
device, you need to set the `Application ID` received from the push-server to the
`applicationid` file located in `/usr/share/ru.auroraos.PushReceiver/`. Further, provided
that you have a valid `Application ID` in the file, when you first launch this application,
it will be registered on the push-server and assigned a `Registration ID`, which will be needed
in `PushSender`. You can copy it by clicking on the button to the right of the output field.

  **Pay attention. This file is used for demonstration purposes only. For business applications,
  you need to get the `Application ID` from the application server.**  

- To configure the service to work with a push server, see [Features](#features).  

- The next step is to download the sources of the `PushSender` application, build the
application and install it on the device. On the first page, you must specify the
`Registration ID`, which can be obtained from the `PushReceiver` (described in the previous
paragraph). On the second page of the application, you can import a configuration `yaml` file
through the top drop-down menu.

## Features

1. Aurora OS versions starting from 5.0.1.  
For correct operation with Push notifications the device must be registered in Aurora Center. Also for correct operation it is necessary 
Aurora Center client, which is up-to-date for the OS version, must be installed on the device. In this case the config for push service will be filled automatically. 

2.  Aurora OS versions lower than 5.0.1.  
For correct work with notifications it is necessary to configure the push service - set adress, port and set the flag for crlValidation to false. 
To get the current settings, you can use the command 
`devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon -m ru.omprussia.PushDaemon.GetNetworkConfiguration`. 
To set the parameters: `devel-su gdbus call -y -d ru.omprussia.PushDaemon -o /ru/omprussia/PushDaemon
-m ru.omprussia.PushDaemon.SetNetworkConfiguration “{‘address’:<‘push-server.ru’>,
'port':<8000>,'crlValidation':<false>"}` (it is important that the host is protocol-less, for example `https://`),
after which `push-daemon` must be restarted using the command
`devel-su systemctl restart push-daemon`.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.PushReceiver.pro](ru.auroraos.PushReceiver.pro)** file describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[components](qml/components)** directory contains the custom QML components.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[PushReceiver.qml](qml/PushReceiver.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.PushReceiver.spec](rpm/ru.auroraos.PushReceiver.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[application](src/application)** directory contains application interface.
  * **[model](src/model)** directory contains model notifications.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.PushReceiver.desktop](ru.auroraos.PushReceiver.desktop)** file defines the display and parameters for launching the application.

**[The DBus-adaptor](src/application/applicationservice.h)** registers the service for processing DBus requests and provides processing of arguments received by the application.
**[The control class](src/application/applicationcontroller.h)** is responsible for displaying the graphical interface, interacting with the push-notification API, and displaying pop-up notifications.
**[The notification model](src/model/notificationsmodel.h)** provides a model of the list of received
push-notifications. They are displayed on the [main](qml/pages/NotificationsPage.qml) page
through the `SilicaListView` component.

## Compatibility

The project is compatible with all current versions of the Aurora OS.

## Screenshots

![screenshots](screenshots/screenshots.png)


## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
