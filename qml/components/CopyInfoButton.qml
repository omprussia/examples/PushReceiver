// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0

Item {
    property string title
    property string value

    objectName: "copyInfoButton"
    anchors {
        left: parent.left
        right: parent.right
    }
    height: Math.max(label.height, button.height)

    Label {
        id: label

        objectName: "label"
        anchors {
            top: parent.top
            left: parent.left
            right: button.left
            rightMargin: Theme.horizontalPageMargin
        }
        wrapMode: Text.WrapAnywhere
        font.pixelSize: Theme.fontSizeExtraSmall
        text: qsTr("%1: <b>%2</b>").arg(title).arg(value)
    }

    Button {
        id: button

        objectName: "button"
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        width: Theme.itemSizeExtraSmall
        height: Theme.itemSizeExtraSmall
        icon {
            source: "image://theme/icon-m-copy"
            sourceSize {
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
            }
        }
        border {
            color: Theme.rgba(color, Theme.opacityFaint)
            highlightColor: Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
        }
        enabled: value

        onClicked: {
            Clipboard.text = value;
            notification.publish();
        }
    }

    Notification {
        id: notification

        objectName: "notification"
        appName: applicationWindow.applicationName
        body: qsTr("'%1' has been copied").arg(title)
        urgency: Notification.Critical
        isTransient: true

        Component.onDestruction: notification.close()
    }
}
