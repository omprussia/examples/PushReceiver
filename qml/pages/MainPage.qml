// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    objectName: "notificationsPage"

    Component {
        id: listViewHeader

        PageHeader {
            objectName: "pageHeader"
            title: qsTr("%1 (%2)").arg(applicationWindow.applicationName).arg(listView.count)
            extraContent.children: [
                IconButton {
                    objectName: "pageHeaderButton"
                    anchors.verticalCenter: parent.verticalCenter
                    icon {
                        source: "image://theme/icon-m-about"
                        sourceSize {
                            width: Theme.iconSizeMedium
                            height: Theme.iconSizeMedium
                        }
                    }

                    onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            ]
        }
    }

    Component {
        id: listViewDelegate

        Item {
            objectName: "listViewDelegate"
            width: parent.width
            height: delegateColumn.height

            Column {
                id: delegateColumn

                objectName: "delegateColumn"
                anchors {
                    left: parent.left
                    right: parent.right
                }

                SectionHeader {
                    objectName: "columnTitleLabel"
                    text: model.title
                }

                Label {
                    objectName: "columnMessageLabel"
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.horizontalPageMargin
                    }
                    font.pixelSize: Theme.fontSizeExtraSmall
                    truncationMode: TruncationMode.Fade
                    text: qsTr("<b>message</b>: %1").arg(model.message)
                }

                Label {
                    objectName: "columnDataLabel"
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.horizontalPageMargin
                    }
                    bottomPadding: Theme.paddingMedium
                    font.pixelSize: Theme.fontSizeExtraSmall
                    truncationMode: TruncationMode.Fade
                    text: qsTr("<b>data</b>: %1").arg(model.data)
                }

                Column {
                    objectName: "itemsSeparatorColumn"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    topPadding: Theme.paddingMedium

                    Separator {
                        objectName: "itemsSeparator"
                        width: parent.width
                        color: Theme.highlightColor
                    }
                }
            }
        }
    }

    SilicaListView {
        id: listView

        objectName: "listView"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: bottomRect.top
        }
        clip: true
        model: ApplicationController.notificationsModel
        header: listViewHeader
        delegate: listViewDelegate
        add: Transition {
            objectName: "addTransition"

            NumberAnimation {
                objectName: "opacityAnimation"
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: 300
            }
            NumberAnimation {
                objectName: "scaleAnimation"
                property: "scale"
                from: 0.0
                to: 1.0
                duration: 300
            }
        }

        ViewPlaceholder {
            objectName: "listViewPlaceholder"
            enabled: listView.count === 0
            text: qsTr("No pushes yet")
        }

        VerticalScrollDecorator {
            objectName: "listViewScrollDecorator"
        }

        PullDownMenu {
            objectName: "pullDownMenu"

            MenuItem {
                objectName: "guideMenuItem"
                text: qsTr("Guide")
                onClicked: pageStack.push(Qt.resolvedUrl("GuidePage.qml"))
            }

            MenuItem {
                objectName: "clearListMenuItem"
                text: qsTr("Clear list")
                onClicked: ApplicationController.notificationsModel.clear()
            }
        }
    }

    Rectangle {
        id: bottomRect

        objectName: "bottomRect"
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: bottomColumn.height
        color: Theme.rgba(Theme.overlayBackgroundColor, Theme.opacityHigh)

        Column {
            id: bottomColumn

            objectName: "bottomColumn"
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: Theme.horizontalPageMargin
                rightMargin: Theme.horizontalPageMargin
            }
            spacing: Theme.paddingLarge
            topPadding: Theme.horizontalPageMargin
            bottomPadding: Theme.horizontalPageMargin

            CopyInfoButton {
                objectName: "appIdLabel"

                title: qsTr("Application ID")
                value: ApplicationController.applicationId
            }

            CopyInfoButton {
                objectName: "regIdLabel"

                title: qsTr("Registration ID")
                value: ApplicationController.registrationId
            }
        }
    }
}
