// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    objectName: "guidePage"
    allowedOrientations: Orientation.All

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: layout.height

        Column {
            id: layout
            objectName: "layout"
            anchors {
                left: parent.left
                right: parent.right
            }
            bottomPadding: Theme.paddingLarge

            PageHeader {
                objectName: "pageHeader"
                title: qsTr("Guide")
            }

            Label {
                objectName: "guideLabel"
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.horizontalPageMargin
                }
                color: palette.highlightColor
                font.pixelSize: Theme.fontSizeSmall
                textFormat: Text.RichText
                wrapMode: Text.WordWrap
                text: qsTr("#guideText")
            }
        }
    }
}
