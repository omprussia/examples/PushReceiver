<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="26"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Приложение позволяет принимать и обрабатывать push-уведомления.&lt;/p&gt;
&lt;p&gt;Оно может быть использовано и как пример взаимодействия с API (получение и отображение уведомлений, запуск приложения из фона и переключение режима интерфейса пользователя), и в утилитарных целях для тестирования и отладки инфраструктуры.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>3-Clause BSD License</source>
        <translation>Лицензия 3-Clause BSD</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2023 Open Mobile Platform LLC&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
	&lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
	&lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
	&lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ApplicationController</name>
    <message>
        <location filename="../src/application/applicationcontroller.cpp" line="65"/>
        <source>Open app</source>
        <translation>Открыть приложение</translation>
    </message>
    <message>
        <location filename="../src/application/applicationcontroller.cpp" line="70"/>
        <source>Push Receiver</source>
        <translation>Получение пушей</translation>
    </message>
</context>
<context>
    <name>CopyInfoButton</name>
    <message>
        <location filename="../qml/components/CopyInfoButton.qml" line="31"/>
        <source>%1: &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>%1: &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/components/CopyInfoButton.qml" line="68"/>
        <source>&apos;%1&apos; has been copied</source>
        <translation>&apos;%1&apos; был скопирован</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>Push Receiver</source>
        <translation>Получение пушей</translation>
    </message>
</context>
<context>
    <name>GuidePage</name>
    <message>
        <location filename="../qml/pages/GuidePage.qml" line="27"/>
        <source>Guide</source>
        <translation>Руководство</translation>
    </message>
    <message>
        <location filename="../qml/pages/GuidePage.qml" line="41"/>
        <source>#guideText</source>
        <translation>&lt;p&gt;При получении уведомления от пуш-сервера первым делом происходит
                            проверка запущен ли на данный момент &lt;code&gt;Push Reciever&lt;/code&gt;. Если он
                            не запущен, то производится его запуск в фоновом режиме (без GUI). Далее
                            через &lt;code&gt;push-daemon API&lt;/code&gt; производится получение всех доставленных
                            на устройство уведомлений и для каждого из них создаются графические
                            представления через &lt;code&gt;notifications API&lt;/code&gt;. Если приложение
                            запущенно в фоне и в течение некоторого времени для него не приходило
                            никаких уведомлений, то оно завершает свою работу. Графическое представление
                            уведомлений может как отображаться как всплывающее, так и помещаться в
                            шторку уведомлений. По нажатию на любое из них будет либо произведен
                            запуск приложения с графическим интерфейсом, если оно не было запущено,
                            либо для текущего процесса отобразится графический интерфейс.&lt;/p&gt;
                            &lt;p&gt;Само приложение представляет из себя одну страницу с двумя секциями.
                            Верхняя часть — это список полученных данным экземпляром приложения
                            пуш-уведомлений и заголовок в котором выводится количество отображаемых
                            уведомлений. В нижней части выводятся &lt;code&gt;Application ID&lt;/code&gt; и
                            &lt;code&gt;Registration ID&lt;/code&gt;. В приложении также есть верхнее выезжающее
                            меню; через него можно очистить список пуш-уведомлений.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="16"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="67"/>
        <source>&lt;b&gt;message&lt;/b&gt;: %1</source>
        <translation>&lt;b&gt;сообщение&lt;/b&gt;: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <source>&lt;b&gt;data&lt;/b&gt;: %1</source>
        <translation>&lt;b&gt;данные&lt;/b&gt;: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="138"/>
        <source>No pushes yet</source>
        <translation>Пушей еще нет</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="150"/>
        <source>Guide</source>
        <translation>Руководство</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="156"/>
        <source>Clear list</source>
        <translation>Очистить список</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="191"/>
        <source>Application ID</source>
        <translation>Application ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="198"/>
        <source>Registration ID</source>
        <translation>Registration ID</translation>
    </message>
</context>
<context>
    <name>PushReceiver</name>
    <message>
        <location filename="../qml/PushReceiver.qml" line="10"/>
        <source>Push Receiver</source>
        <translation>Получение пушей</translation>
    </message>
</context>
</TS>
