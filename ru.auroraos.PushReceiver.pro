# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.PushReceiver

QT += dbus

CONFIG += \
    auroraapp \
    
PKGCONFIG += \
    pushclient \
    nemonotifications-qt5 \

HEADERS += \
    src/application/applicationcontroller.h \
    src/application/applicationservice.h \
    src/model/notificationsmodel.h \

SOURCES += \
    src/application/applicationcontroller.cpp \
    src/application/applicationservice.cpp \
    src/model/notificationsmodel.cpp \
    src/main.cpp \

DISTFILES += \
    ru.auroraos.PushReceiver.desktop \
    rpm/ru.auroraos.PushReceiver.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \
    README.ru.md \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.PushReceiver.ts \
    translations/ru.auroraos.PushReceiver-ru.ts \

APP_ID = testApp # write down the application_id here
APP_ID_PATH = /usr/share/$${TARGET}
APP_ID_FILE = applicationid

applicationid_install.extra = echo $${APP_ID} > $${OUT_PWD}/$${APP_ID_FILE}
applicationid_install.files = $${OUT_PWD}/$${APP_ID_FILE}
applicationid_install.path = $${APP_ID_PATH}
applicationid_install.CONFIG = no_check_exist

INSTALLS += \
    applicationid_install \

DEFINES += \
    DBUS_SERVICE=\\\"ru.auroraos.PushReceiver\\\" \
    DBUS_PATH=\\\"/ru/auroraos/PushReceiver\\\" \
    DBUS_INTERFACE=\\\"ru.auroraos.PushReceiver\\\" \
    APP_ID_FILE_PATH=\\\"$${APP_ID_PATH}/$${APP_ID_FILE}\\\" \
