// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QGuiApplication>
#include <auroraapp.h>

#include "application/applicationcontroller.h"
#include "application/applicationservice.h"

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("PushReceiver"));

    QStringList applicationArgs = application->arguments();
    applicationArgs.removeFirst();

    if (ApplicationService::isRegistered()) {
        return ApplicationService::updateApplicationArgs(applicationArgs);
    } else {
        QScopedPointer<ApplicationService> applicationService(
                new ApplicationService(application.data()));
        QScopedPointer<ApplicationController> applicationController(
                new ApplicationController(applicationArgs, application.data()));

        QObject::connect(applicationService.data(), &ApplicationService::guiRequested,
                         applicationController.data(), &ApplicationController::showGui);

        return application->exec();
    }
}
