// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QFile>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QDebug>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <nemonotifications-qt5/notification.h>

#include "applicationcontroller.h"
#include "applicationservice.h"
#include "../model/notificationsmodel.h"

using namespace Aurora::PushNotifications;

ApplicationController::ApplicationController(QObject *parent)
    : ApplicationController({}, parent) { }

ApplicationController::ApplicationController(const QStringList &arguments, QObject *parent)
    : QObject(parent),
      m_notificationsClient(new Client(this)),
      m_notificationsModel(new NotificationsModel(this))
{
    qRegisterMetaType<NotificationsModel *>("NotificationsModel *");

    _setApplicationId(_readApplicationId());

    if (arguments.indexOf(QStringLiteral("/no-gui")) == -1)
        showGui();

    connect(m_notificationsClient, &Client::clientInactive, this,
            &ApplicationController::quitOnClientInactive);
    connect(m_notificationsClient, &Client::pushSystemReadinessChanged, [](bool status) {
        qWarning() << "Push system is" << (status ? "available" : "not available");
    });
    connect(m_notificationsClient, &Client::registrationId, this,
            &ApplicationController::_setRegistrationId);
    connect(m_notificationsClient, &Client::registrationError,
            []() { qWarning() << "Push system have problems with registrationId"; });
    connect(m_notificationsClient, &Client::notifications, this,
            &ApplicationController::pushListToNotificationList);
}

void ApplicationController::quitOnClientInactive()
{
    if (!m_view)
        qApp->quit();
}

void ApplicationController::pushListToNotificationList(const PushList &pushList)
{
    for (const auto &push : pushList) {
        m_notificationsModel->insertPush(push);

        QJsonDocument jsonDcoument = QJsonDocument::fromJson(push.data.toUtf8());
        QString notifyType = jsonDcoument.object().value("mtype").toString();

        if (notifyType == QStringLiteral("action"))
            continue;

        static QVariant defaultAction = Notification::remoteAction(
                QStringLiteral("default"), tr("Open app"), ApplicationService::notifyDBusService(),
                ApplicationService::notifyDBusPath(), ApplicationService::notifyDBusIface(),
                ApplicationService::notifyDBusMethod());

        Notification notification;
        notification.setAppName(tr("Push Receiver"));
        notification.setSummary(push.title);
        notification.setBody(push.message);
        notification.setIsTransient(false);
        notification.setItemCount(1);
        notification.setHintValue("x-nemo-feedback", "sms_exists");
        notification.setRemoteAction(defaultAction);
        notification.setUrgency(Notification::Urgency::Critical);
        notification.publish();
    }
}

NotificationsModel *ApplicationController::notificationsModel() const
{
    return m_notificationsModel;
}

QString ApplicationController::applicationId() const
{
    return m_notificationsClient->applicationId();
}

QString ApplicationController::registrationId() const
{
    return m_registrationId;
}

void ApplicationController::showGui()
{
    if (m_view) {
        m_view->raise();
        m_view->showFullScreen();
    } else {
        m_view = Aurora::Application::createView();
        m_view->rootContext()->setContextProperty(QStringLiteral("ApplicationController"), this);
        m_view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/PushReceiver.qml")));
        m_view->show();
    }
}

void ApplicationController::_setApplicationId(const QString &applicationId)
{
    if (m_notificationsClient->applicationId() == applicationId)
        return;

    m_notificationsClient->setApplicationId(applicationId);
    m_notificationsClient->registrate();

    emit applicationIdChanged(applicationId);
}

void ApplicationController::_setRegistrationId(const QString &registrationId)
{
    if (registrationId == m_registrationId)
        return;

    m_registrationId = registrationId;

    emit registrationIdChanged(registrationId);
}

QString ApplicationController::_readApplicationId() const
{
    QFile applicationIdFile(QStringLiteral(APP_ID_FILE_PATH));
    return applicationIdFile.exists() && applicationIdFile.open(QIODevice::ReadOnly)
            ? applicationIdFile.readAll().trimmed()
            : QString();
}
