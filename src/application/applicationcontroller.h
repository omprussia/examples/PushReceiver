// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef APPLICATIONCONTROLLER_H
#define APPLICATIONCONTROLLER_H

#include <QtCore/QObject>

#include <push_client.h>
#include <auroraapp.h>

class QQuickView;
class NotificationsModel;
class ApplicationController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(NotificationsModel *notificationsModel READ notificationsModel CONSTANT)
    Q_PROPERTY(QString applicationId READ applicationId NOTIFY applicationIdChanged)
    Q_PROPERTY(QString registrationId READ registrationId NOTIFY registrationIdChanged)

public:
    explicit ApplicationController(QObject *parent = nullptr);
    ApplicationController(const QStringList &arguments, QObject *parent = nullptr);

    NotificationsModel *notificationsModel() const;
    QString applicationId() const;
    QString registrationId() const;

signals:
    void applicationIdChanged(const QString &applicationId);
    void registrationIdChanged(const QString &registrationId);

public slots:
    void showGui();

private slots:
    void _setApplicationId(const QString &applicationId);
    void _setRegistrationId(const QString &registrationId);
    void pushListToNotificationList(const Aurora::PushNotifications::PushList &pushList);
    void quitOnClientInactive();

private:
    QString _readApplicationId() const;

private:
    Aurora::PushNotifications::Client *m_notificationsClient{ nullptr };
    NotificationsModel *m_notificationsModel{ nullptr };
    QQuickView *m_view{ nullptr };

    QString m_registrationId{};
};

#endif // APPLICATIONCONTROLLER_H
