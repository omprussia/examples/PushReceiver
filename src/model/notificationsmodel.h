// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NOTIFICATIONSMODEL_H
#define NOTIFICATIONSMODEL_H

#include <QtCore/QList>
#include <QtCore/QAbstractListModel>

#include <push_types.h>

class NotificationsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit NotificationsModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &index = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public slots:
    void insertPush(const Aurora::PushNotifications::Push &push);
    void clear();

private:
    QList<Aurora::PushNotifications::Push> m_notifications{};
};

#endif // NOTIFICATIONSMODEL_H
