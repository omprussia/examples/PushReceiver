// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "notificationsmodel.h"

using namespace Aurora::PushNotifications;

enum NotificationRole {
    TitleRole = Qt::UserRole,
    DataRole,
    ActionRole,
    MessageRole,
    SoundRole,
    ImageRole,
    ExpiredRole,
    CategoryRole,
    PriorityRole,
};

NotificationsModel::NotificationsModel(QObject *parent) : QAbstractListModel(parent) { }

int NotificationsModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index)

    return m_notifications.count();
}

QVariant NotificationsModel::data(const QModelIndex &index, const int role) const
{
    if (!index.isValid() || index.row() > m_notifications.size())
        return QVariant();

    auto notification = m_notifications.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
    case TitleRole:
        return notification.title;
    case DataRole:
        return notification.data;
    case ActionRole:
        return notification.action;
    case MessageRole:
        return notification.message;
    case SoundRole:
        return notification.sound;
    case ImageRole:
        return notification.image;
    case CategoryRole:
        return notification.categoryId;
    }

    return QVariant();
}

QHash<int, QByteArray> NotificationsModel::roleNames() const
{
    static QHash<int, QByteArray> roles = {
        { TitleRole, "title" },       { DataRole, "data" },   { ActionRole, "action" },
        { MessageRole, "message" },   { SoundRole, "sound" }, { ImageRole, "image" },
        { CategoryRole, "category" },
    };

    return roles;
}

void NotificationsModel::insertPush(const Push &push)
{
    beginInsertRows(QModelIndex(), 0, 0);
    m_notifications.insert(0, push);
    endInsertRows();
}

void NotificationsModel::clear()
{
    beginResetModel();
    m_notifications.clear();
    endResetModel();
}
